from threading import Thread
from time import sleep
from fractions import Fraction

from gi.repository import GLib

from .hyper_params import *
from .utils import adjust_sign

class AntManager(Thread):
	sugarhills = []

	def __init__(self, ants, gui, **kwargs):
		super().__init__(**kwargs)
		self.ants = ants
		self.gui = gui

	def run(self):
		sleep(.5)
		while True:
			for ant in self.ants:
				GLib.idle_add(self.move, ant)
				GLib.idle_add(self.detect_sugar, ant)
				if ant.detected_sugarhill and ant.count != ant.detected_sugarhill.weight:
					GLib.idle_add(self.alarm_close_ants, ant)
			sleep(.01)

	def move(self, ant):
		if self.is_at_border(ant):
			ant.x_dir, ant.y_dir = ant.get_random_direction()
		if ant.transporting_sugar:
			self.change_dir(ant, self.gui.ANTHILL_POSITION)
			print(ant.x, ant.y, ant.x_dir, ant.y_dir, self.gui.ANTHILL_POSITION)
			if self.is_at_anthill(ant):
				self.unload_sugar(ant)
		elif ant.detected_sugarhill and ant.detected_sugarhill.weight <= ant.count:
			self.change_dir(ant, [ant.detected_sugarhill.x, ant.detected_sugarhill.y])
			print(ant.x, ant.y, ant.x_dir, ant.y_dir)
			print('sugarhill', [ant.detected_sugarhill.x, ant.detected_sugarhill.y])

		if self.is_at_border(ant):
			ant.x_dir, ant.y_dir = ant.get_random_direction()
		GLib.idle_add(self.gui.move_ant, ant, ant.x + ant.x_dir, ant.y + ant.y_dir)

	def is_at_border(self, ant):
		dimension = self.gui.get_size()
		x = ant.x + 10 * ant.x_dir
		y = ant.y + 10 * ant.y_dir
		return (
			x >= dimension.width - BORDER_DIST
			or x <= BORDER_DIST
			or y >= dimension.height - BORDER_DIST
			or y <= BORDER_DIST
		)

	def detect_sugar(self, ant):
		for sugarhill in self.sugarhills:
			if abs(ant.x - sugarhill.x) <= DETECTION_DIST and abs(ant.y - sugarhill.y) <= DETECTION_DIST:
				if sugarhill.detected == True:
					if ant.detected_sugarhill == sugarhill:
						if ant.detected_sugarhill.weight <= ant.count:
							self.transport_sugar(ant)
				else:
					ant.detected_sugarhill = sugarhill
					ant.has_detected_sugar.set_visible(True)
					sugarhill.detected = True
					if ant.detected_sugarhill.weight == ant.count:
						self.transport_sugar(ant)

	def transport_sugar(self, ant):
		GLib.idle_add(self.gui.main_container.remove, ant.detected_sugarhill)
		ant.sugar.set_visible(True)
		ant.has_detected_sugar.set_visible(False)
		self.change_dir(ant, self.gui.ANTHILL_POSITION)
		ant.transporting_sugar = True

	def change_dir(self, ant, position):
		x_diff = position[0] - ant.x
		y_diff = position[1] - ant.y
		print('x-diff', x_diff, 'y-diff', y_diff)
		if y_diff == 0:
			ant.y_dir = 0
			ant.x_dir = x_diff
			return

		m = x_diff / y_diff
		direction = Fraction(m).limit_denominator(10)
		print('direction', direction)
		ant.x_dir = direction.numerator
		ant.y_dir = direction.denominator

		ant.x_dir = adjust_sign(x_diff, ant.x_dir)
		ant.y_dir = adjust_sign(y_diff, ant.y_dir)

	def is_at_anthill(self, ant):
		return (
			abs(ant.x - self.gui.ANTHILL_POSITION[0]) <= AT_ANTHILL_DIST
			and abs(ant.y - self.gui.ANTHILL_POSITION[1]) <= AT_ANTHILL_DIST
		)

	def alarm_close_ants(self, ant):
		others = list(self.ants)
		others.remove(ant)
		for other in others:
			if (
				abs(ant.x - other.x) < ALARM_DIST
				and abs(ant.y - other.y) < ALARM_DIST
			):
				GLib.idle_add(ant.incr_count)
				self.ants.remove(other)
				GLib.idle_add(other.set_visible, False)

	def unload_sugar(self, ant):
		ant.detected_sugarhill = None
		ant.sugar.set_visible(False)
		while ant.count > 1:
			GLib.idle_add(self.gui.add_ant, self.gui.ANTHILL_POSITION)
			new = self.ants[-1]
			new.x_dir, new.y_dir = new.get_random_direction()
			ant.count -= 1
		ant.x_dir, ant.y_dir = ant.get_random_direction()
		ant.transporting_sugar = False
