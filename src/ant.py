import random

from gi.repository import Gtk

@Gtk.Template(resource_path='/de/hszg/sugarants/ant.ui')
class Ant(Gtk.Fixed):
	__gtype_name__='Ant'

	count_label = Gtk.Template.Child()
	sugar = Gtk.Template.Child()
	has_detected_sugar = Gtk.Template.Child()

	__count = 0
	detected_sugarhill = None
	transporting_sugar = False

	def __init__(self, count, coordinates, **kwargs):
		super().__init__(**kwargs)
		self.count = count
		self.x, self.y = coordinates
		self.x_dir, self.y_dir = self.get_random_direction()

	def get_random_direction(self):
		x_dir = random.randint(-7, 7)
		y_dir = random.randint(-7, 7)
		if x_dir == 0 and y_dir == 0:
			return self.get_random_direction()
		else:
			return (x_dir, y_dir)

	@property
	def count(self):
		return self.__count

	@count.setter
	def count(self, new:int):
		self.__count = new
		if new > 1:
			self.count_label.set_text(str(self.__count))
		else:
			self.count_label.set_text('')

	def incr_count(self):
		self.count += 1
