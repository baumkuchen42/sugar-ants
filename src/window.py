# window.py
#
# Copyright 2021 Uta
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import random, time

from gi.repository import Gtk, GLib
from .utils import load_css
from .sugarhill import SugarHill
from .ant import Ant
from .ant_manager import AntManager
from .hyper_params import *


@Gtk.Template(resource_path='/de/hszg/sugarants/window.ui')
class SugarantsTask6Window(Gtk.ApplicationWindow):
	__gtype_name__ = 'SugarantsTask6Window'

	main_container = Gtk.Template.Child()
	nr_ants_label = Gtk.Template.Child()
	add_ants_btn = Gtk.Template.Child()
	spawn_sugarhill_btn = Gtk.Template.Child()
	sugarhill_popover = Gtk.Template.Child()
	sugarhill_weight_entry = Gtk.Template.Child()
	sugarhill_create_btn = Gtk.Template.Child()
	anthill = Gtk.Template.Child()

	ant_manager = NotImplemented

	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.maximize()
		load_css()
		self.connect_events()
		self.start()

	def connect_events(self):
		self.add_ants_btn.connect('clicked', self.on_add_ants_btn_clicked)
		self.spawn_sugarhill_btn.connect('clicked', self.show_sugarhill_popover)
		self.sugarhill_create_btn.connect('clicked', self.spawn_sugarhill)

	def start(self):
		self.ANTHILL_POSITION = [BORDER_DIST+MIN_DIST, BORDER_DIST+MIN_DIST]
		self.main_container.move(self.anthill, self.ANTHILL_POSITION[0], self.ANTHILL_POSITION[1])
		self.spawn_ants(int(self.nr_ants_label.get_text()))
		self.anthill.set_visible(True)

	def on_add_ants_btn_clicked(self, widget=None):
		current_amount = int(self.nr_ants_label.get_text())
		self.nr_ants_label.set_text(str(current_amount+1))
		self.add_ant(self.get_random_position())

	def add_ant(self, position):
		ant = Ant(1, position)
		self.main_container.put(ant, ant.x, ant.y)
		GLib.idle_add(self.ant_manager.ants.append, ant)

	def spawn_ants(self, amount):
		ant_list = []
		for i in range(amount):
			ant = Ant(1, self.ANTHILL_POSITION)
			self.main_container.put(ant, ant.x, ant.y)
			ant_list.append(ant)
		self.ant_manager = AntManager(ant_list, self, daemon=True)
		self.ant_manager.start()

	def show_sugarhill_popover(self, widget=None):
		self.sugarhill_popover.show()

	def spawn_sugarhill(self, widget=None):
		weight = int(self.sugarhill_weight_entry.get_text())
		sugarhill = SugarHill(weight, self.get_random_position())
		self.sugarhill_popover.hide()
		self.main_container.put(sugarhill, sugarhill.x, sugarhill.y)
		self.ant_manager.sugarhills.append(sugarhill)

	def get_random_position(self):
		width, height = self.get_size()
		x = random.randint(BORDER_DIST, width-BORDER_DIST)
		y = random.randint(BORDER_DIST, height-BORDER_DIST)

		for child in self.main_container.get_children():
			if type(child) == Ant or type(child) == SugarHill:
				if abs(x - child.x) < MIN_DIST and abs(y - child.y) < MIN_DIST:
					return self.get_random_position()

		return (x, y)

	def move_ant(self, ant, x, y):
		ant.x = x
		ant.y = y
		self.main_container.move(ant, ant.x, ant.y)
