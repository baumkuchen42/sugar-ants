from gi.repository import Gtk

@Gtk.Template(resource_path='/de/hszg/sugarants/sugarhill.ui')
class SugarHill(Gtk.Box):
	__gtype_name__='SugarHillBox'

	weight_label = Gtk.Template.Child()
	__weight = 0
	detected = False

	def __init__(self, weight, coordinates, **kwargs):
		super().__init__(**kwargs)
		self.weight = weight
		self.x, self.y = coordinates

	@property
	def weight(self):
		return self.__weight

	@weight.setter
	def weight(self, new:int):
		self.__weight = new
		self.weight_label.set_text(str(self.__weight))
